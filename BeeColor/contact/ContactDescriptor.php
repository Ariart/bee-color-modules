<?php

namespace wfw\modules\BeeColor\contact;

use wfw\engine\core\conf\ModuleDescriptor;
use wfw\modules\BeeColor\contact\data\model\ContactModel;
use wfw\modules\BeeColor\contact\data\model\ContactModelAccess;
use wfw\modules\BeeColor\contact\data\model\IContactModelAccess;
use wfw\modules\BeeColor\contact\domain\repository\ContactRepository;
use wfw\modules\BeeColor\contact\domain\repository\IContactRepository;
use wfw\modules\BeeColor\contact\security\ContactAccessControlPolicies;

/**
 * Contact module descriptor
 */
final class ContactDescriptor extends ModuleDescriptor {
	/**
	 * @return array
	 */
	public static function di(): array {
		return [
			IContactRepository::class => ['instanceOf'=>ContactRepository::class,'shared'=>true],
			IContactModelAccess::class => ['instanceOf'=>ContactModelAccess::class,'shared'=>true]
		];
	}

	/**
	 * @return array
	 * @throws \ReflectionException
	 */
	public static function cleanablePaths(): array {
		return [ self::root() ];
	}

	/**
	 * @return array
	 */
	public static function securityPolicies(): array {
		return [ ContactAccessControlPolicies::class ];
	}

	/**
	 * @return array
	 */
	public static function models(): array {
		return [
			ContactModel::class => []
		];
	}
}