<?php
namespace wfw\modules\BeeColor\contact\command\handlers;

use wfw\engine\core\command\ICommand;
use wfw\engine\lib\PHP\types\UUID;
use wfw\modules\BeeColor\contact\command\CreateContact;
use wfw\modules\BeeColor\contact\domain\Contact;

/**
 * Traite la commande de création d'une nouvelle prise de contact
 */
final class CreateContactHandler extends ContactCommandHandler {

	/**
	 * Traite la commande
	 *
	 * @param ICommand $command Commande à traiter
	 */
	public function handleCommand(ICommand $command) {
		/** @var CreateContact $command */
		$this->repos()->add(new Contact(
			new UUID(),
			$command->getLabel(),
			$command->getInfos()
		),$command);
	}
}