<?php
namespace wfw\modules\BeeColor\contact\domain\events;

/**
 * La prise de contact a été marquée comme lue
 */
final class MarkedAsReadEvent extends ContactEvent{}