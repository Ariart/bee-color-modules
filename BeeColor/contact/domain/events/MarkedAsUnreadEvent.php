<?php
namespace wfw\modules\BeeColor\contact\domain\events;

/**
 * La prise de contact est marquée comme non lu.
 */
final class MarkedAsUnreadEvent extends ContactEvent {}