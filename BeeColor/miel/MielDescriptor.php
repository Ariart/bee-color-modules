<?php

namespace wfw\modules\BeeColor\miel;

use wfw\engine\core\conf\ModuleDescriptor;
use wfw\modules\BeeColor\miel\security\MielAccessControlPolicies;

/**
 * Miel descriptor module
 */
class MielDescriptor extends ModuleDescriptor {
	/**
	 * @return array
	 */
	public static function securityPolicies(): array {
		return [ MielAccessControlPolicies::class ];
	}
}