<?php

use wfw\engine\core\conf\WFW;
use wfw\modules\BeeColor\miel\MielDescriptor;

WFW::registerModules(MielDescriptor::class);