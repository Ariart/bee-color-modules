<?php

namespace wfw\modules\BeeColor\news\cache;

/**
 * Comme il est difficile d'utiliser l'autoload pour seulement charger des constantes,
 * on utilise une classe.
 */
final class NewsCacheKeys {
	public const ROOT="WFW/modules/BeeColor/news";
}