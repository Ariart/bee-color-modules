<?php
namespace wfw\modules\BeeColor\news\domain\errors;

/**
 * Levée par un article
 */
class ArticleFailure extends \Exception {}