<?php
namespace wfw\modules\BeeColor\news\domain\errors;

/**
 * Levée lorsqu'un article ne peut pas être mis en ligne.
 */
final class PutOnlineFailed extends ArticleFailure {}