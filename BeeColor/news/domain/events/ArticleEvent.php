<?php
namespace wfw\modules\BeeColor\news\domain\events;

use wfw\engine\core\domain\events\DomainEvent;

/**
 * Evenement concernant un article
 */
abstract class ArticleEvent extends DomainEvent {}