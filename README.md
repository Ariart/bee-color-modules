# Modules that can be added to a wfw project

## What is a wfw project ?

Please see [the wfw framework core repository](https://framagit.org/Ariart/bee-color-framework)

# Bee-color modules for the wfw framework

- contact -> allow you to create contact form and use the ContactModel to get a copy of all forms submitted.
- news -> allow to manage a simple blog.
- miel -> allow a logged user to change some info in the site/app.

# How to install any module

In your wfw project, create a `modules` directory and past the wanted module code inside.

**Important note :** by default, a module is not enabled for security concerns.

# How to enable a module ?

In your `site/config/conf.json`, enable your module editing the `server/packages` array to add
`modules/theModule`.

**Note :** the `modules/[path]` must match the module namespace :

	- `modules/contact` for namespace `wfw\modules\contact`
	- `modules/beeColor/contact` for namespace `wfw\modules\beeColor\contact`

# How to create a module

To create a module, just create a project and make it's namespace start by `wfw\modules\[vendor\moduleName]` or
`wfw\modules\moduleName`.

**Note :** A good practice may be to add a vendor prefix in front of all your modules, to avoid name colisions
if you want to install another module.

Then create a class in the root directory of your module that extends `wfw\core\conf\ModuleDescriptor`.

Let's take the `wfw\modules\BeeColor\Contact` module as an example :

```php
<?php

namespace wfw\BeeColor\contact;

use wfw\engine\core\conf\ModuleDescriptor;

final class ContactDescriptor extends ModuleDescriptor {

}
```

And create a php file `module.registration.php` :

```php
<?php

use wfw\BeeColor\contact\ContactDescriptor;
use wfw\engine\core\conf\WFW;

WFW::registerModules(ContactDescriptor::class);
```

To get more informations about modules descriptors, please refer to the [wfw documentation](https://wfwdoc.bee-color.fr)

**Warning :** The current documentation is not up to date. I do my best to create a good starting point that should be available
arround october 2019.